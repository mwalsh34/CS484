#include "mp2-helper.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Sequential solver using red-black method
void sequential_solver(int asize) {

  int p_asize = asize+2;
  float *A;
  init_array(asize,&A);
  int it, i, j;

  for(it = 0; it < MAXIT; ++it) {
    reset_peak_seq(asize, &A);
    // Red
    bool alt = false;
    for (i = 1; i < p_asize-1; i++) {
      for (j = !alt ? 1 : 2; j < p_asize-1; j+=2) {
        A[IND(i,j,p_asize)] = 0.20*(
        A[IND(i,j,p_asize)] +
        A[IND(i-1,j,p_asize)] +
        A[IND(i+1,j,p_asize)] +
        A[IND(i,j-1,p_asize)] +
        A[IND(i,j+1,p_asize)]);
      }
      alt = !alt;
    }
    
    // Black
    alt = true;
    for (i = 1; i < p_asize-1; i++) {
      for (j = !alt ? 1 : 2; j < p_asize-1; j+=2) {
        A[IND(i,j,p_asize)] = 0.20*(
	A[IND(i,j,p_asize)] +
        A[IND(i-1,j,p_asize)] +
        A[IND(i+1,j,p_asize)] +
        A[IND(i,j-1,p_asize)] +
        A[IND(i,j+1,p_asize)]);
      }
      alt = !alt;
    }

    float iteration_sum = 0.0f;

    #ifdef REPORT_SUM
    for(i = 1; i < p_asize-1; i++)
      for(j = 1; j < p_asize-1; j++)
        iteration_sum += A[IND(i,j,p_asize)];
    #endif

    report_iteration(-1, it, iteration_sum, &A);
  }

  clean_arrays(&A);
}

// Parallel solver using MPI and red-black method
void parallel_solver(int asize, int tileSizeX, int tileSizeY) {
  int myRank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  int p_asize = asize+2;

  // Subarray initialization with padding
  // Use A for the output array
  float *A;
  init_subarrays(myRank, tileSizeX, tileSizeY, &A);

  int it;
  for(it = 0; it < MAXIT; it++)
  {
    // Reset peak at the beginning of each iteration
    reset_peak(myRank, tileSizeX, &A);
	int numRows = (asize / tileSizeY);
	int numCols = (asize / tileSizeX);
	int myRow = myRank / numCols;
	int myCol = myRank - (myRow * numCols);

	// Determining rank of neighboring processes
	int rankUp = myRank - (asize / tileSizeX);
	int rankDown = myRank - (asize / tileSizeX);
	int rankLeft = myRank - 1;
	int rankRight = myRank + 1;

	// Checking if this process has logical neighbors or not, processes handling tiles at the edges of the array will not need info from certain neighbors
	if (myRow == 0){
	rankUp = myRank;
	}
	if (myRow == (asize / tileSizeY)-1){
	rankDown = myRank;
	}
	if (myCol == 0){
	rankLeft = myRank;
	}
	if (myCol == (asize / tileSizeX) - 1){
	rankRight = myRank;
	}

	float * myUpData;
		float * myDownData;
			float * myLeftData;
				float * myRightData;
					myUpData = (float*)NULL;
						myDownData = (float*)NULL;
							myLeftData = (float*)NULL;
								myRightData = (float*)NULL;

	if (rankUp != myRank){
			myUpData = (float*)malloc(sizeof(float)*(tileSizeY+2));
				}
					if (rankDown != myRank){
							myDownData = (float*)malloc(sizeof(float)*(tileSizeY+2));
}
if (rankLeft != myRank){
		myLeftData = (float*)malloc(sizeof(float)*(tileSizeX+2));
			}
				if (rankRight != myRank){
						myRightData = (float*)malloc(sizeof(float)*(tileSizeX+2));
							}
								int i;
									if (myUpData != NULL){
											myUpData[0] = myUpData[tileSizeY+1] = 1.0f;
													for (i = 1; i <tileSizeY + 1; i++){
																myUpData[i] = A[IND(i, 1, tileSizeX+2 )];
																		}
																			}
																				if (myDownData != NULL){
																						myDownData[0] = myUpData[tileSizeY+1] = 1.0f;
																								for (i = 1; i < tileSizeY +1; i++){
																											myDownData[i] = A[IND(i, tileSizeX, tileSizeX+2)];
																													}
																														}
																															if (myLeftData != NULL){
																																	myLeftData[0] = myLeftData[tileSizeX+1] = 1.0f;
																																			for (i = 1; i < tileSizeX + 1; i++){
																																						myLeftData[i] = A[IND(1, i, tileSizeX + 2)];
																																								}
																																									}
																																										if (myRightData != NULL){
																																												myRightData[0] = myRightData[tileSizeX+1] = 1.0f;
																																														for (i = 1; i < tileSizeX +1; i++){
																																																	myRightData[i] = A[IND(tileSizeY, i, tileSizeX + 2)];
																																																			}
																																																				}




																																									float * upData;
																																										float * downData;
																																											float * leftData;
																																												float * rightData;
																																													upData = (float*)NULL;
																																														downData = (float*)NULL;
																																															leftData = (float*)NULL;
																																																rightData = (float*)NULL;
																																																	if (rankUp != myRank){
																																																			upData = (float*)malloc(sizeof(float)*(tileSizeY+2));
																																																					upData[0] = upData[tileSizeY+1] = 1.0f;
																																																						}
																																																							if (rankDown != myRank){
																																																									downData = (float*)malloc(sizeof(float)*(tileSizeY+2));
																																																											downData[0] = downData[tileSizeY+1] = 1.0f;
																																																												}
																																																													if (rankLeft != myRank){
																																																															leftData = (float*)malloc(sizeof(float)*(tileSizeX+2));
																																																																	leftData[0] = leftData[tileSizeX+1] = 1.0f;
																																																																		}
																																																																			if(rankRight != myRank){
																																																																					rightData = (float*)malloc(sizeof(float)*(tileSizeX+2));
																																																																							rightData[0] = rightData[tileSizeX+1] = 1.0f;
																																																																								}
																																																													int ierr;
																																																														if (myUpData != NULL){
																																																																ierr = MPI_Send(&myUpData, (tileSizeY+2), MPI_FLOAT, rankUp, 0, MPI_COMM_WORLD);
																																																																	}
																																																																		if (myDownData != NULL){
																																																																				ierr = MPI_Send(&myDownData, (tileSizeY+2), MPI_FLOAT, rankDown, 0, MPI_COMM_WORLD);
																																																																					}
																																																																						if (myLeftData != NULL){
																																																																								ierr = MPI_Send(&myLeftData, (tileSizeX+2), MPI_FLOAT, rankLeft, 0, MPI_COMM_WORLD);
																																																																									}
																																																																										if (myRightData != NULL){
																																																																												ierr  = MPI_Send(&myRightData, (tileSizeX+2), MPI_FLOAT, rankRight, 0, MPI_COMM_WORLD);
																																																																													}
																																																																														MPI_Status status;
																																																																														MPI_Barrier(MPI_COMM_WORLD);
										MPI_Request request;																																																																					if (upData != NULL){
																																																																																	ierr = MPI_Irecv(&upData, (tileSizeY+2), MPI_FLOAT, rankUp, 0, MPI_COMM_WORLD, &request);	
																																																																														}
																																																																																			if (downData != NULL){
																																																																																					ierr = MPI_Irecv(&downData, (tileSizeY+2), MPI_FLOAT, rankDown, 0, MPI_COMM_WORLD, &request);
																																																																																						}
																																																																																							if (leftData != NULL){
																																																																																									ierr = MPI_Irecv(&leftData, (tileSizeX+2), MPI_FLOAT, rankLeft, 0, MPI_COMM_WORLD, &request);
																																																																																										}
																																																																																											if (rightData != NULL){
																																																																																													ierr = MPI_Irecv(&rightData, (tileSizeX+2), MPI_FLOAT, rankRight, 0, MPI_COMM_WORLD, &request);
																																																																																														}
																																																																																											bool alt = false;
																																																																																												int upCell, downCell, rightCell, leftCell;
																																																																																													int j;
																																																																																														for (i = 1; i < MIN((tileSizeX + 1), (p_asize-1)); i++){
																																																																																																for (j = !alt ? 1 : 2; j < MIN((tileSizeY + 1),(p_asize-1)); j+=2){
																																																																																																			if ((i == 1) && upData != NULL){
																																																																																																							upCell = upData[j];
																																																																																																										}
																																																																																																													else {
																																																																																																																	upCell = A[IND(i-1, j, p_asize)];
																																																																																																																				}
																																																																																																																							if ((i == tileSizeX || i == p_asize-2) && downData != NULL){
																																																																																																																											downCell = downData[j];
																																																																																																																														}
																																																																																																																																	else {
																																																																																																																																					downCell = A[IND(i+1, j, p_asize)];
																																																																																																																																								}
																																																																																																																																											if ((j == 1) && leftData != NULL){
																																																																																																																																															leftCell = leftData[i];
																																																																																																																																																		}
																																																																																																																																																					else {
																																																																																																																																																									leftCell = A[IND(i, j-1, p_asize)];
																																																																																																																																																												}
																																																																																																																																																															if ((j == tileSizeY || j ==p_asize-2) && rightData != NULL){
																																																																																																																																																																			rightCell = rightData[i];
																																																																																																																																																																						}
																																																																																																																																																																									else {
																																																																																																																																																																													rightCell = A[IND(i, j +1, p_asize)];
																																																																																																																																																																																}

																																																																																																																																																																																			A[IND(i, j, p_asize)] = 0.20*(
																																																																																																																																																																																						A[IND(i, j, p_asize)] +
																																																																																																																																																																																									upCell +
																																																																																																																																																																																												downCell +
																																																																																																																																																																																															leftCell +
																																																																																																																																																																																																		rightCell
																																																																																																																																																																																																					);
																																																																																																																																																																																																							}
																																																																																																																																																																																																										alt = !alt;
																																																																																																																																																																																																											}
																																																																																																																																																																																																									if (myUpData != NULL){
																																																																																																																																																																																																											ierr = MPI_Send(&myUpData, (tileSizeY+2), MPI_FLOAT, rankUp, 0, MPI_COMM_WORLD);
																																																																																																																																																																																																												}
																																																																																																																																																																																																													if (myDownData != NULL){
																																																																																																																																																																																																															ierr = MPI_Send(&myDownData, (tileSizeY+2), MPI_FLOAT, rankDown, 0, MPI_COMM_WORLD);
																																																																																																																																																																																																																}
																																																																																																																																																																																																																	if (myLeftData != NULL){
																																																																																																																																																																																																																			ierr = MPI_Send(&myLeftData, (tileSizeX+2), MPI_FLOAT, rankLeft, 0 ,MPI_COMM_WORLD);
																																																																																																																																																																																																																				}
																																																																																																																																																																																																																					if (myRightData != NULL){
																																																																																																																																																																																																																							ierr = MPI_Send(&myRightData, (tileSizeX+2), MPI_FLOAT, rankRight, 0, MPI_COMM_WORLD);
																																																																																																																																																																																																																								}
																																																																																	MPI_Barrier(MPI_COMM_WORLD);																																																																																																																																								if (upData != NULL){
																																																																																																																																																																																																																											ierr = MPI_Irecv(&upData, (tileSizeY+2), MPI_FLOAT, rankUp, 0, MPI_COMM_WORLD, &request);
																																																																																																																																																																																																																												}
																																																																																																																																																																																																																													if (downData != NULL){
																																																																																																																																																																																																																															ierr = MPI_Irecv(&downData, (tileSizeY+2), MPI_FLOAT, rankDown, 0, MPI_COMM_WORLD, &request);
																																																																																																																																																																																																																																}
																																																																																																																																																																																																																																	if (leftData != NULL){
																																																																																																																																																																																																																																			ierr = MPI_Irecv(&leftData, (tileSizeX+2), MPI_FLOAT, rankLeft, 0, MPI_COMM_WORLD, &request);
																																																																																																																																																																																																																																				}
																																																																																																																																																																																																																																					if (rightData != NULL){
																																																																																																																																																																																																																																							ierr = MPI_Irecv(&rightData, (tileSizeX+2), MPI_FLOAT, rankRight, 0, MPI_COMM_WORLD, &request);
																																																																																																																																																																																																																																								}
																																																																																																																																																																																																																												alt = true;
																																																																																																																																																																																																																													for (i = 1; i < MIN(p_asize-1, tileSizeX+1); i++){
																																																																																																																																																																																																																															for (j = !alt ? 1 : 2; j < MIN(p_asize-1, tileSizeY+1); j+=2){
																																																																																																																																																																																																																																		if (i==1 && upData != NULL){
																																																																																																																																																																																																																																						upCell = upData[j];
																																																																																																																																																																																																																																									}
																																																																																																																																																																																																																																												else {
																																																																																																																																																																																																																																																upCell = A[IND(i-1, j, p_asize)];
																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																						if ((i==tileSizeX || i==p_asize-2) && downData != NULL){
																																																																																																																																																																																																																																																										downCell = downData[j];
																																																																																																																																																																																																																																																													}
																																																																																																																																																																																																																																																																else {
																																																																																																																																																																																																																																																																				downCell = A[IND(i+1, j, p_asize)];
																																																																																																																																																																																																																																																																							}
																																																																																																																																																																																																																																																																										if (j==1 && leftData != NULL){
																																																																																																																																																																																																																																																																														leftCell = leftData[i];
																																																																																																																																																																																																																																																																																	}
																																																																																																																																																																																																																																																																																				else {
																																																																																																																																																																																																																																																																																								leftCell = A[IND(i, j-1, p_asize)];
																																																																																																																																																																																																																																																																																											}
																																																																																																																																																																																																																																																																																														if ((j==tileSizeY || j==p_asize-2) && rightData != NULL){
																																																																																																																																																																																																																																																																																																		rightCell = rightData[i];
																																																																																																																																																																																																																																																																																																					}
																																																																																																																																																																																																																																																																																																								else {
																																																																																																																																																																																																																																																																																																												rightCell = A[IND(i, j+1, p_asize)];
																																																																																																																																																																																																																																																																																																															}
																																																																																																																																																																																																																																																																																																																		A[IND(i,j,p_asize)] = 0.20 *(
																																																																																																																																																																																																																																																																																																																					A[IND(i,j,p_asize)] +
																																																																																																																																																																																																																																																																																																																								upCell +
																																																																																																																																																																																																																																																																																																																											downCell +
																																																																																																																																																																																																																																																																																																																														leftCell+
																																																																																																																																																																																																																																																																																																																																	rightCell
																																																																																																																																																																																																																																																																																																																																				);
																																																																																																																																																																																																																																																																																																																																						}
																																																																																																																																																																																																																																																																																																																																								alt = !alt;
																																																																																																																																																																																																																																																																																																																																									}
																																																																																																																																																																																																																																																																																																																																float iter_sum = 0.0f;
																																																																																																																																																																																																																																																																																																																																    for (i = 1; i < tileSizeX+1; i++){
																																																																																																																																																																																																																																																																																																																																    	for (j = 1; j < tileSizeY+1; j++){
																																																																																																																																																																																																																																																																																																																																			iter_sum += A[IND(i,j,p_asize)];
																																																																																																																																																																																																																																																																																																																																				}
																																																																																																																																																																																																																																																																																																																													    }
																																																																																																																																																																																																																																																																																																																																				        	if (myRank != 0){
																																																																																																																																																																																																																																																																																																																																								ierr = MPI_Send(&iter_sum, 1, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
																																																																																																																																																																																																																																																																																																																																									}



    /* INSTRUCTION: Add code below that will aggregate sum of all processes'
     *              subarrays into the variable iter_sum
     */
    //float iter_sum = 0.0f;
	MPI_Barrier(MPI_COMM_WORLD);
    #ifdef REPORT_SUM
      /*
       *  Sum all cells in all processes and
       *  store it to Rank 0's iter_sum
       */
      MPI_Request request1;
	int num_procs;
		MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
			int proc_sum;
				MPI_Status status2;
				      	for (i = 1; i < num_procs; i++){
							ierr = MPI_Irecv(&proc_sum, 1, MPI_FLOAT, i, 0, MPI_COMM_WORLD, &request1);
									iter_sum += proc_sum;
										}



    #endif

    // Report on iteration: DO NOT REMOVE THIS LINE
    report_iteration(myRank, it, iter_sum, &A);
  }
  
  clean_arrays(&A);
}

int main(int argc, char** argv) {

  int asize = 8;
  int tile_size_x = 4;
  int tile_size_y = 4;
  
  int nProcs;
  int myRank;
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

  if (argc < 4) {
    if(!myRank) {
      printf("USAGE: mpirun -np <Number of Processes> ./mp2 <Matrix Dimension> <Tile Size X> <Tile Size Y>\n");
      MPI_Finalize();
      return -1;
    }
  }

  if (argc == 4) {
    asize = atoi(argv[1]);
    tile_size_x = atoi(argv[2]);
    tile_size_y = atoi(argv[3]);
  }

  // Run and time the sequential version
 
  #ifdef RUN_SEQ
  if(!myRank) {
    time_seqn(asize);
  }

  // Let sequential finish
  MPI_Barrier(MPI_COMM_WORLD);
  #endif

  // Run and time the parallel version
  time_parallel(myRank, asize, tile_size_x, tile_size_y);

  MPI_Finalize();
  return 0;
}
